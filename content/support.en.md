+++
title = "Support"
slug = "support"
thumbnail = "images/tn.png"
description = "support"
+++

We are a non-profit and we depend on donations. Please help us preserve the awesomeness by donating.
## Donations
<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M22 9.761c0 .536-.065 1.084-.169 1.627-.847 4.419-3.746 5.946-7.449 5.946h-.572c-.453 0-.838.334-.908.789l-.803 5.09c-.071.453-.456.787-.908.787h-2.736c-.39 0-.688-.348-.628-.732l1.386-8.88.062-.056h2.155c5.235 0 8.509-2.618 9.473-7.568.812.814 1.097 1.876 1.097 2.997zm-14.216 4.252c.116-.826.459-1.177 1.385-1.179l2.26-.002c4.574 0 7.198-2.09 8.023-6.39.8-4.134-2.102-6.442-6.031-6.442h-7.344c-.517 0-.958.382-1.038.901-2.304 14.835-2.97 18.607-3.038 19.758-.021.362.269.672.635.672h3.989l1.159-7.318z"/></svg>
[Paypal](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=donate%40hackerspace.gr&item_name=Donation&currency_code=EUR)

<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="7.846 7.798 47.363 47.34"><path fill="#FF5900" d="M7.846 30.818c0-11.027 8.96-21.366 20.775-22.843 8.468-.985 14.868 2.265 19.298 6.498 4.135 3.938 6.598 8.96 7.188 14.868.492 5.907-.787 11.027-4.233 15.852-3.446 4.727-10.24 9.945-18.708 9.945h-11.52V32.394c.099-5.022 1.772-9.354 7.877-11.422 5.317-1.575 11.52 1.378 13.391 6.991 1.969 6.006-.887 10.043-4.234 12.505s-8.566 2.462-12.012.099v7.778c2.265 1.083 5.12 1.378 7.188 1.28 7.482-1.084 13.292-5.317 15.754-11.717 2.561-6.794.787-14.671-4.529-19.594-6.4-5.219-13.391-6.499-20.874-2.855-5.218 2.659-8.861 8.074-9.748 13.981v25.699H7.945l-.099-24.321z"/></svg>
[Patreon](https://www.patreon.com/hackerspacegr)

IBAN: GR3201720820005082079678770

## Equipment

You can donate equipment to be utilized by people and projects. We are open to all new equipment, but you can have a look on our [Wishlist](https://www.hackerspace.gr/wiki/Wishlist).