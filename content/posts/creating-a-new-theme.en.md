+++ 
date = "2023-11-17"
title = "Hackerspace.gr has a new website!"
slug = "hackerspace-gr-new-website" 
+++


We made an organised effort to improve our website. As a result we migrated to hugo and decided to open the repo publicly.

You can find it on [Gitlab](https://gitlab.com/hsgr/website).